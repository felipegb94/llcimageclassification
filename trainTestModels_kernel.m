scenes = {'CALsuburb', 'MITcoast', 'MITforest', 'MIThighway', 'MITinsidecity', 'MITmountain', 'MITopencountry', 'MITstreet', 'PARoffice', 'bedroom', 'industrial', 'kitchen', 'livingroom', 'MITtallbuilding','store'};

% Construct instance matrix
cols = size(CALsuburb_K,2);
instanceMatrix = zeros(1500, cols);
currRow = 1;
for s = scenes
    % Make Variable using scene names
    var_name = strcat(s{1}, '_K'); 
    var = eval(genvarname(var_name)); 
    instanceMatrix(currRow:currRow+99,:) = var;
    currRow = currRow+100;
end

% Construct labels vector
train_labels = zeros(15*100,1);
currRow = 1;
for i = 1:15
    train_labels(currRow:currRow+99,1) = ones(100,1)*i;
    currRow = currRow+100;
end

disp('Training model with pyramid features...');
instanceMatrix = sparse(instanceMatrix);
model = svmtrain(train_labels, instanceMatrix);

%% Testing
disp('Testing model with pyramid features...');

%numZeros = size(store_K_test,1) +size(MITtallbuilding_K_test,1) +size(livingroom_K_test,1) +size(kitchen_K_test,1) +size(industrial_K_test,1) +size(bedroom_K_test,1) +size(PARoffice_K_test,1) +size(MITstreet_K_test,1) +size(MITopencountry_K_test,1) +size(MITmountain_K_test,1) +size(MITinsidecity_K_test,1) + size(MITcoast_K_test,1) + size(MITforest_K_test,1) + size(MIThighway_K_test,1);
counter = 1;
test_labels = cell(1,15);
for s = scenes
    var_name = strcat(s{1}, '_K_test');
    var = size(eval(genvarname(var_name)), 1);
    test_labels{1, counter} = ones(var,1)*counter;
    counter = counter+1;
end

predictions = cell(1,15);
counter = 1;
for s = scenes
    var_name = strcat(s{1}, '_K_test');
    predictions{1,counter} =  svmpredict(test_labels{1,counter}, sparse(eval(genvarname(var_name))), model);
    counter = counter+1;
end

disp('Building Confusion Matrix');
confusion_matrix = zeros(15, 15);
for i = 1:15
    confusion_matrix(i,:) = getConfusionMatrixRow(predictions{1,i},15);
end
disp(confusion_matrix)
% save('results/baseline_confusion_matrix.mat','confusion_matrix');






