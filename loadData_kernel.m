%% Load Training pyramids

disp('Loading Training pyramids');
load('kernelTrain/CALsuburb_trainKernel.mat');
CALsuburb_K = K;
%CALsuburb_K2 = K2;
%CALsuburb_K3 = K3;

load('kernelTrain/MITcoast_trainKernel.mat');
MITcoast_K = K;
%MITcoast_K2 = K2;
%MITcoast_K3 = K3;

load('kernelTrain/MITforest_trainKernel.mat');
MITforest_K = K;
%MITforest_K2 = K2;
%MITforest_K3 = K3;

load('kernelTrain/MIThighway_trainKernel.mat');
MIThighway_K = K;
%MIThighway_K2 = K2;
%MIThighway_K3 = K3;

load('kernelTrain/MITinsidecity_trainKernel.mat');
MITinsidecity_K = K;
%MITinsidecity_K2 = K2;
%MITinsidecity_K3 = K3;

load('kernelTrain/MITmountain_trainKernel.mat');
MITmountain_K = K;
%MITmountain_K2 = K2;
%MITmountain_K3 = K3;

load('kernelTrain/MITopencountry_trainKernel.mat');
MITopencountry_K = K;
%MITopencountry_K2 = K2;
%MITopencountry_K3 = K3;

load('kernelTrain/MITstreet_trainKernel.mat');
MITstreet_K = K;
%MITstreet_K2 = K2;
%MITstreet_K3 = K3;

load('kernelTrain/MITtallbuilding_trainKernel.mat');
MITtallbuilding_K = K;
%MITtallbuilding_K2 = K2;
%MITtallbuilding_K3 = K3;

load('kernelTrain/PARoffice_trainKernel.mat');
PARoffice_K = K;
%PARoffice_K2 = K2;
%PARoffice_K3 = K3;

load('kernelTrain/bedroom_trainKernel.mat');
bedroom_K = K;
%bedroom_K2 = K2;
%bedroom_K3 = K3;

load('kernelTrain/industrial_trainKernel.mat');
industrial_K = K;
%industrial_K2 = K2;
%industrial_K3 = K3;

load('kernelTrain/kitchen_trainKernel.mat');
kitchen_K = K;
%kitchen_K2 = K2;
%kitchen_K3 = K3;

load('kernelTrain/livingroom_trainKernel.mat');
livingroom_K = K;
%livingroom_K2 = K2;
%livingroom_K3 = K3;

load('kernelTrain/store_trainKernel.mat');
store_K = K;
%store_K2 = K2;
%store_K3 = K3;

%% Load Testing pyramids

disp('Loading Testing pyramids');

load('kernelTest/CALsuburb_testKernel.mat');
CALsuburb_K_test = K;
% CALsuburb_K2_test = K2;
% CALsuburb_K3_test = K3;

load('kernelTest/MITcoast_testKernel.mat');
MITcoast_K_test = K;
% MITcoast_K2_test = K2;
% MITcoast_K3_test = K3;

load('kernelTest/MITforest_testKernel.mat');
MITforest_K_test = K;
% MITforest_K2_test = K2;
% MITforest_K3_test = K3;

load('kernelTest/MIThighway_testKernel.mat');
MIThighway_K_test = K;
% MIThighway_K2_test = K2;
% MIThighway_K3_test = K3;

load('kernelTest/MITinsidecity_testKernel.mat');
MITinsidecity_K_test = K;
% MITinsidecity_K2_test = K2;
% MITinsidecity_K3_test = K3;

load('kernelTest/MITmountain_testKernel.mat');
MITmountain_K_test = K;
% MITmountain_K2_test = K2;
% MITmountain_K3_test = K3;

load('kernelTest/MITopencountry_testKernel.mat');
MITopencountry_K_test = K;
% MITopencountry_K2_test = K2;
% MITopencountry_K3_test = K3;

load('kernelTest/MITstreet_testKernel.mat');
MITstreet_K_test = K;
% MITstreet_K2_test = K2;
% MITstreet_K3_test = K3;

load('kernelTest/MITtallbuilding_testKernel.mat');
MITtallbuilding_K_test = K;
% MITtallbuilding_K2_test = K2;
% MITtallbuilding_K3_test = K3;

load('kernelTest/PARoffice_testKernel.mat');
PARoffice_K_test = K;
% PARoffice_K2_test = K2;
% PARoffice_K3_test = K3;

load('kernelTest/bedroom_testKernel.mat');
bedroom_K_test = K;
% bedroom_K2_test = K2;
% bedroom_K3_test = K3;

load('kernelTest/industrial_testKernel.mat');
industrial_K_test = K;
% industrial_K2_test = K2;
% industrial_K3_test = K3;

load('kernelTest/kitchen_testKernel.mat');
kitchen_K_test = K;
% kitchen_K2_test = K2;
% kitchen_K3_test = K3;

load('kernelTest/livingroom_testKernel.mat');
livingroom_K_test = K;
% livingroom_K2_test = K2;
% livingroom_K3_test = K3;

load('kernelTest/store_testKernel.mat');
store_K_test = K;
% store_K2_test = K2;
% store_K3_test = K3;

