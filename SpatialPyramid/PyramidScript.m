image_folder = '../scene_categories/';
directories = {'CALsuburb', 'MITcoast', 'MITforest', 'MIThighway', 'MITinsidecity', 'MITmountain', 'MITopencountry', 'MITstreet', 'PARoffice', 'bedroom', 'industrial', 'kitchen', 'livingroom', 'MITtallbuilding','store'};


%% Generate dictionary
disp('Creating dictionary for all training images...');

% BuildDictionary_LLC(directories);

for s = directories
    
    fprintf('Loading train images from %s...\n', s{1});
    images_dir = strcat(image_folder, s{1}, '_train');
    data_dir = strcat('../data/',s{1});

    fnames = dir(fullfile(images_dir, '*.jpg'));
    num_files = size(fnames,1);
    filenames = cell(num_files,1);

    for f = 1:num_files
        filenames{f} = fnames(f).name;
    end
    
    fprintf('Contructing train spatial pyramids for %s...\n', s{1});

    % return pyramid descriptors for all files in filenames
    pyramid_all = BuildPyramid(filenames,images_dir,data_dir);

    
    filename = strcat('../train_pyramids/',s{1}, '_trainPyramid.mat');
    
    save(filename,'pyramid_all');
    
    %fprintf('Computing histogram intersection kernel for %s...\n', s{1});
    % compute histogram intersection kernel
    load(filename)
    K = hist_isect(pyramid_all, pyramid_all); 
    
    filename = strcat('../kernelTrain/',s{1}, '_trainKernel.mat');

    save(filename,'K');
end


for s = directories
    
    fprintf('Loading test images from %s...\n', s{1});
    images_dir = strcat(image_folder, s{1}, '_test');
    data_dir = strcat('../data/',s{1});

    
    fnames = dir(fullfile(images_dir, '*.jpg'));
    num_files = size(fnames,1);
    filenames = cell(num_files,1);

    for f = 1:num_files
        filenames{f} = fnames(f).name;
    end
    
    fprintf('Contructing test spatial pyramids for %s...\n', s{1});

    % return pyramid descriptors for all files in filenames
    %pyramid_all = BuildPyramid(filenames,images_dir,data_dir);

    filename = strcat('../train_pyramids/',s{1}, '_trainPyramid.mat');
    
    save(filename,'pyramid_all');
  
    %fprintf('Computing histogram intersection kernel for %s...\n', s{1});
    % compute histogram intersection kernel
    load(filename);
    K = hist_isect(pyramid_all, pyramid_all); 
    
    filename = strcat('../kernelTest/',s{1}, '_testKernel.mat');

    save(filename,'K');
end







