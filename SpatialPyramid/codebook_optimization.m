function [ B ] = codebook_optimization(dictionary, sift_all, lambda, sigma )
%codebook_optimization 
%   Iterative codebook optimization

M = size(dictionary,1); % Number of codes in codebook/dictionary)
D = size(dictionary,2); % Dimensionality of code
N = size(sift_all, 2); % Number of sift features

B = dictionary;
X = sift_all;
for i = 1:N
    d = zeros(1, M);
    xi = X(i, :);
    
    % Locality Constraint Parameter.
    for j = 1:M
        bj = B(j, :);
        distance = norm(xi-bj);
        denominator = exp(-1*distance/sigma);
        d(1, j) = 1 / denominator;
    end
    
    % Normalize d between 0 and 1.
    d = d/norm(d);
    
    % Coding descriptor

    z = B - repmat(xi, M, 1);
    C = z*z';
    C = C + lambda*diag(d);
    ci = C\ones(M,1);
    ci = ci/sum(ci);
    
    % Remove bias: Find entries in codebook where ci > 0.01
    id = [];
    for j = 1:size(ci)
        
        if(abs(ci(j)) > 0.01)
            id = [id, j];
        end
        
    end
    
    Bi = B(id, :);
    numEntries = size(id,2);

    identity = eye(numEntries);
    z = Bi - repmat(xi, numEntries, 1);
    C = z*z';
    C = C + identity*lambda*trace(C);
    ci_prime = C\ones(numEntries,1);
    ci_prime = ci_prime/sum(ci_prime);
    
    %Update Basis
    
    deltaB = -2 * ci_prime * (xi - (Bi'*ci_prime)');
    mu = sqrt(1/i);
    Bi = Bi - (mu*deltaB/norm(ci_prime));
    B(id, :) = Bi;
    
end

end

