function [ pyramid_all ] = BuildPyramid_LLC( imageFileList, imageBaseDir, dataBaseDir, params, canSkip, saveSift  )
%BuildPyramid_LLC Summary of this function goes here
%   Detailed explanation goes here

%% parameters for feature extraction (see GenerateSiftDescriptors)

if(~exist('params','var'))
    params.maxImageSize = 1000
    params.gridSpacing = 8
    params.patchSize = 16
    params.dictionarySize = 1024
    params.numTextonImages = 100
    params.pyramidLevels = 3
    params.oldSift = false;
end


if(~isfield(params,'maxImageSize'))
    params.maxImageSize = 1000
end
if(~isfield(params,'gridSpacing'))
    params.gridSpacing = 8;
end
if(~isfield(params,'patchSize'))
    params.patchSize = 16
end
if(~isfield(params,'dictionarySize'))
    params.dictionarySize = 1024
end
if(~isfield(params,'numTextonImages'))
    params.numTextonImages = 100
end
if(~isfield(params,'pyramidLevels'))
    params.pyramidLevels = 3
end
if(~isfield(params,'oldSift'))
    params.oldSift = false
end

if(~exist('canSkip','var'))
    canSkip = 0
end
if(~exist('saveSift','var'))
    saveSift = 1
end

k = 3;

pfig = sp_progress_bar('Building Spatial Pyramid');
%% build the pyramid
if(saveSift)
    %GenerateSiftDescriptors( imageFileList,imageBaseDir,dataBaseDir,params,canSkip,pfig);
end
%CalculateDictionary(imageFileList,imageBaseDir,dataBaseDir,'_sift.mat',params,canSkip,pfig);
BuildHistograms_LLC2(imageFileList,imageBaseDir,dataBaseDir,'_sift.mat',params,canSkip, pfig);
pyramid_all = CompilePyramid_LLC(imageFileList,dataBaseDir,sprintf('_texton_ind_%d_LLC.mat',params.dictionarySize),params,canSkip, pfig, k);
close(pfig);

end

