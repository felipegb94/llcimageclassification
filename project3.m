%% CS 766 Project 3 - Locality-constrained Linear Coding for Scene Classification
% Isaac Sung and Felipe Gutierrez
%


run('config.m');
run('SpatialPyramid/PyramidScript_LLC.m');
run('loadData_LLC.m');
run('trainTestModels.m')
