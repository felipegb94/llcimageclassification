function [ row ] = getConfusionMatrixRow( prediction, numLabels )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

total = size(prediction, 1);
row = zeros(1, numLabels);

for i = 1:total
    label = prediction(i, 1);
    row(1, label) = row(1, label)+1;
end

row = (row/total)*100;

end

