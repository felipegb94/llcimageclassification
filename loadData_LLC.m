%% Load Training pyramids

train_dir = 'llc_train_pyramids/';
test_dir = 'llc_test_pyramids/';

disp('Loading Training pyramids');
load(strcat(train_dir, 'CALsuburb_trainPyramid_LLC.mat'));
CALsuburb_pyramid_all = pyramid_all;
%CALsuburb_pyramid_all2 = pyramid_all2;
%CALsuburb_pyramid_all3 = pyramid_all3;

load(strcat(train_dir, 'MITcoast_trainPyramid_LLC.mat'));
MITcoast_pyramid_all = pyramid_all;
%MITcoast_pyramid_all2 = pyramid_all2;
%MITcoast_pyramid_all3 = pyramid_all3;

load(strcat(train_dir, 'MITforest_trainPyramid_LLC.mat'));
MITforest_pyramid_all = pyramid_all;
%MITforest_pyramid_all2 = pyramid_all2;
%MITforest_pyramid_all3 = pyramid_all3;

load(strcat(train_dir, 'MIThighway_trainPyramid_LLC.mat'));
MIThighway_pyramid_all = pyramid_all;
% MIThighway_pyramid_all2 = pyramid_all2;
% MIThighway_pyramid_all3 = pyramid_all3;

load(strcat(train_dir, 'MITinsidecity_trainPyramid_LLC.mat'));
MITinsidecity_pyramid_all = pyramid_all;
%MITinsidecity_pyramid_all2 = pyramid_all2;
%MITinsidecity_pyramid_all3 = pyramid_all3;

load(strcat(train_dir, 'MITmountain_trainPyramid_LLC.mat'));
MITmountain_pyramid_all = pyramid_all;
%MITmountain_pyramid_all2 = pyramid_all2;
%MITmountain_pyramid_all3 = pyramid_all3;

load(strcat(train_dir, 'MITopencountry_trainPyramid_LLC.mat'));
MITopencountry_pyramid_all = pyramid_all;
%MITopencountry_pyramid_all2 = pyramid_all2;
%MITopencountry_pyramid_all3 = pyramid_all3;

load(strcat(train_dir, 'MITstreet_trainPyramid_LLC.mat'));
MITstreet_pyramid_all = pyramid_all;
%MITstreet_pyramid_all2 = pyramid_all2;
%MITstreet_pyramid_all3 = pyramid_all3;

load(strcat(train_dir, 'MITtallbuilding_trainPyramid_LLC.mat'));
MITtallbuilding_pyramid_all = pyramid_all;
%MITtallbuilding_pyramid_all2 = pyramid_all2;
%MITtallbuilding_pyramid_all3 = pyramid_all3;

load(strcat(train_dir, 'PARoffice_trainPyramid_LLC.mat'));
PARoffice_pyramid_all = pyramid_all;
%PARoffice_pyramid_all2 = pyramid_all2;
%PARoffice_pyramid_all3 = pyramid_all3;

load(strcat(train_dir, 'bedroom_trainPyramid_LLC.mat'));
bedroom_pyramid_all = pyramid_all;
%bedroom_pyramid_all2 = pyramid_all2;
%bedroom_pyramid_all3 = pyramid_all3;

load(strcat(train_dir, 'industrial_trainPyramid_LLC.mat'));
industrial_pyramid_all = pyramid_all;
%industrial_pyramid_all2 = pyramid_all2;
%industrial_pyramid_all3 = pyramid_all3;

load(strcat(train_dir, 'kitchen_trainPyramid_LLC.mat'));
kitchen_pyramid_all = pyramid_all;
%kitchen_pyramid_all2 = pyramid_all2;
%kitchen_pyramid_all3 = pyramid_all3;

load(strcat(train_dir, 'livingroom_trainPyramid_LLC.mat'));
livingroom_pyramid_all = pyramid_all;
%livingroom_pyramid_all2 = pyramid_all2;
%livingroom_pyramid_all3 = pyramid_all3;

load(strcat(train_dir, 'store_trainPyramid_LLC.mat'));
store_pyramid_all = pyramid_all;
%store_pyramid_all2 = pyramid_all2;
%store_pyramid_all3 = pyramid_all3;

%% Load Testing pyramids

disp('Loading Testing pyramids');
% 
load(strcat(test_dir, 'CALsuburb_testPyramid_LLC.mat'));
CALsuburb_pyramid_all_test = pyramid_all;
% CALsuburb_pyramid_all2_test = pyramid_all2;
% CALsuburb_pyramid_all3_test = pyramid_all3;

load(strcat(test_dir, 'MITcoast_testPyramid_LLC.mat'));
MITcoast_pyramid_all_test = pyramid_all;
% MITcoast_pyramid_all2_test = pyramid_all2;
% MITcoast_pyramid_all3_test = pyramid_all3;

load(strcat(test_dir, 'MITforest_testPyramid_LLC.mat'));
MITforest_pyramid_all_test = pyramid_all;
% MITforest_pyramid_all2_test = pyramid_all2;
% MITforest_pyramid_all3_test = pyramid_all3;
% 
load(strcat(test_dir, 'MIThighway_testPyramid_LLC.mat'));
MIThighway_pyramid_all_test = pyramid_all;
% MIThighway_pyramid_all2_test = pyramid_all2;
% MIThighway_pyramid_all3_test = pyramid_all3;

load(strcat(test_dir, 'MITinsidecity_testPyramid_LLC.mat'));
MITinsidecity_pyramid_all_test = pyramid_all;
% MITinsidecity_pyramid_all2_test = pyramid_all2;
% MITinsidecity_pyramid_all3_test = pyramid_all3;

load(strcat(test_dir, 'MITmountain_testPyramid_LLC.mat'));
MITmountain_pyramid_all_test = pyramid_all;
% MITmountain_pyramid_all2_test = pyramid_all2;
% MITmountain_pyramid_all3_test = pyramid_all3;

load(strcat(test_dir, 'MITopencountry_testPyramid_LLC.mat'));
MITopencountry_pyramid_all_test = pyramid_all;
% MITopencountry_pyramid_all2_test = pyramid_all2;
% MITopencountry_pyramid_all3_test = pyramid_all3;

load(strcat(test_dir, 'MITstreet_testPyramid_LLC.mat'));
MITstreet_pyramid_all_test = pyramid_all;
% MITstreet_pyramid_all2_test = pyramid_all2;
% MITstreet_pyramid_all3_test = pyramid_all3;
% 
load(strcat(test_dir, 'MITtallbuilding_testPyramid_LLC.mat'));
MITtallbuilding_pyramid_all_test = pyramid_all;
% MITtallbuilding_pyramid_all2_test = pyramid_all2;
% MITtallbuilding_pyramid_all3_test = pyramid_all3;

load(strcat(test_dir, 'PARoffice_testPyramid_LLC.mat'));
PARoffice_pyramid_all_test = pyramid_all;
% PARoffice_pyramid_all2_test = pyramid_all2;
% PARoffice_pyramid_all3_test = pyramid_all3;

load(strcat(test_dir, 'bedroom_testPyramid_LLC.mat'));
bedroom_pyramid_all_test = pyramid_all;
% bedroom_pyramid_all2_test = pyramid_all2;
% bedroom_pyramid_all3_test = pyramid_all3;

load(strcat(test_dir, 'industrial_testPyramid_LLC.mat'));
industrial_pyramid_all_test = pyramid_all;
% industrial_pyramid_all2_test = pyramid_all2;
% industrial_pyramid_all3_test = pyramid_all3;

load(strcat(test_dir, 'kitchen_testPyramid_LLC.mat'));
kitchen_pyramid_all_test = pyramid_all;
% kitchen_pyramid_all2_test = pyramid_all2;
% kitchen_pyramid_all3_test = pyramid_all3;

load(strcat(test_dir, 'livingroom_testPyramid_LLC.mat'));
livingroom_pyramid_all_test = pyramid_all;
% livingroom_pyramid_all2_test = pyramid_all2;
% livingroom_pyramid_all3_test = pyramid_all3;

load(strcat(test_dir, 'store_testPyramid_LLC.mat'));
store_pyramid_all_test = pyramid_all;
% store_pyramid_all2_test = pyramid_all2;
% store_pyramid_all3_test = pyramid_all3;

