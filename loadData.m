%% Load Training pyramids

disp('Loading Training pyramids');
load('train_pyramids/CALsuburb_trainPyramid.mat');
CALsuburb_pyramid_all = pyramid_all;
%CALsuburb_pyramid_all2 = pyramid_all2;
%CALsuburb_pyramid_all3 = pyramid_all3;

load('train_pyramids/MITcoast_trainPyramid.mat');
MITcoast_pyramid_all = pyramid_all;
%MITcoast_pyramid_all2 = pyramid_all2;
%MITcoast_pyramid_all3 = pyramid_all3;

load('train_pyramids/MITforest_trainPyramid.mat');
MITforest_pyramid_all = pyramid_all;
%MITforest_pyramid_all2 = pyramid_all2;
%MITforest_pyramid_all3 = pyramid_all3;

load('train_pyramids/MIThighway_trainPyramid.mat');
MIThighway_pyramid_all = pyramid_all;
%MIThighway_pyramid_all2 = pyramid_all2;
%MIThighway_pyramid_all3 = pyramid_all3;

load('train_pyramids/MITinsidecity_trainPyramid.mat');
MITinsidecity_pyramid_all = pyramid_all;
%MITinsidecity_pyramid_all2 = pyramid_all2;
%MITinsidecity_pyramid_all3 = pyramid_all3;

load('train_pyramids/MITmountain_trainPyramid.mat');
MITmountain_pyramid_all = pyramid_all;
%MITmountain_pyramid_all2 = pyramid_all2;
%MITmountain_pyramid_all3 = pyramid_all3;

load('train_pyramids/MITopencountry_trainPyramid.mat');
MITopencountry_pyramid_all = pyramid_all;
%MITopencountry_pyramid_all2 = pyramid_all2;
%MITopencountry_pyramid_all3 = pyramid_all3;

load('train_pyramids/MITstreet_trainPyramid.mat');
MITstreet_pyramid_all = pyramid_all;
%MITstreet_pyramid_all2 = pyramid_all2;
%MITstreet_pyramid_all3 = pyramid_all3;

load('train_pyramids/MITtallbuilding_trainPyramid.mat');
MITtallbuilding_pyramid_all = pyramid_all;
%MITtallbuilding_pyramid_all2 = pyramid_all2;
%MITtallbuilding_pyramid_all3 = pyramid_all3;

load('train_pyramids/PARoffice_trainPyramid.mat');
PARoffice_pyramid_all = pyramid_all;
%PARoffice_pyramid_all2 = pyramid_all2;
%PARoffice_pyramid_all3 = pyramid_all3;

load('train_pyramids/bedroom_trainPyramid.mat');
bedroom_pyramid_all = pyramid_all;
%bedroom_pyramid_all2 = pyramid_all2;
%bedroom_pyramid_all3 = pyramid_all3;

load('train_pyramids/industrial_trainPyramid.mat');
industrial_pyramid_all = pyramid_all;
%industrial_pyramid_all2 = pyramid_all2;
%industrial_pyramid_all3 = pyramid_all3;

load('train_pyramids/kitchen_trainPyramid.mat');
kitchen_pyramid_all = pyramid_all;
%kitchen_pyramid_all2 = pyramid_all2;
%kitchen_pyramid_all3 = pyramid_all3;

load('train_pyramids/livingroom_trainPyramid.mat');
livingroom_pyramid_all = pyramid_all;
%livingroom_pyramid_all2 = pyramid_all2;
%livingroom_pyramid_all3 = pyramid_all3;

load('train_pyramids/store_trainPyramid.mat');
store_pyramid_all = pyramid_all;
%store_pyramid_all2 = pyramid_all2;
%store_pyramid_all3 = pyramid_all3;

%% Load Testing pyramids

disp('Loading Testing pyramids');

load('test_pyramids/CALsuburb_testPyramid.mat');
CALsuburb_pyramid_all_test = pyramid_all;
% CALsuburb_pyramid_all2_test = pyramid_all2;
% CALsuburb_pyramid_all3_test = pyramid_all3;

load('test_pyramids/MITcoast_testPyramid.mat');
MITcoast_pyramid_all_test = pyramid_all;
% MITcoast_pyramid_all2_test = pyramid_all2;
% MITcoast_pyramid_all3_test = pyramid_all3;

load('test_pyramids/MITforest_testPyramid.mat');
MITforest_pyramid_all_test = pyramid_all;
% MITforest_pyramid_all2_test = pyramid_all2;
% MITforest_pyramid_all3_test = pyramid_all3;

load('test_pyramids/MIThighway_testPyramid.mat');
MIThighway_pyramid_all_test = pyramid_all;
% MIThighway_pyramid_all2_test = pyramid_all2;
% MIThighway_pyramid_all3_test = pyramid_all3;

load('test_pyramids/MITinsidecity_testPyramid.mat');
MITinsidecity_pyramid_all_test = pyramid_all;
% MITinsidecity_pyramid_all2_test = pyramid_all2;
% MITinsidecity_pyramid_all3_test = pyramid_all3;

load('test_pyramids/MITmountain_testPyramid.mat');
MITmountain_pyramid_all_test = pyramid_all;
% MITmountain_pyramid_all2_test = pyramid_all2;
% MITmountain_pyramid_all3_test = pyramid_all3;

load('test_pyramids/MITopencountry_testPyramid.mat');
MITopencountry_pyramid_all_test = pyramid_all;
% MITopencountry_pyramid_all2_test = pyramid_all2;
% MITopencountry_pyramid_all3_test = pyramid_all3;

load('test_pyramids/MITstreet_testPyramid.mat');
MITstreet_pyramid_all_test = pyramid_all;
% MITstreet_pyramid_all2_test = pyramid_all2;
% MITstreet_pyramid_all3_test = pyramid_all3;

load('test_pyramids/MITtallbuilding_testPyramid.mat');
MITtallbuilding_pyramid_all_test = pyramid_all;
% MITtallbuilding_pyramid_all2_test = pyramid_all2;
% MITtallbuilding_pyramid_all3_test = pyramid_all3;

load('test_pyramids/PARoffice_testPyramid.mat');
PARoffice_pyramid_all_test = pyramid_all;
% PARoffice_pyramid_all2_test = pyramid_all2;
% PARoffice_pyramid_all3_test = pyramid_all3;

load('test_pyramids/bedroom_testPyramid.mat');
bedroom_pyramid_all_test = pyramid_all;
% bedroom_pyramid_all2_test = pyramid_all2;
% bedroom_pyramid_all3_test = pyramid_all3;

load('test_pyramids/industrial_testPyramid.mat');
industrial_pyramid_all_test = pyramid_all;
% industrial_pyramid_all2_test = pyramid_all2;
% industrial_pyramid_all3_test = pyramid_all3;

load('test_pyramids/kitchen_testPyramid.mat');
kitchen_pyramid_all_test = pyramid_all;
% kitchen_pyramid_all2_test = pyramid_all2;
% kitchen_pyramid_all3_test = pyramid_all3;

load('test_pyramids/livingroom_testPyramid.mat');
livingroom_pyramid_all_test = pyramid_all;
% livingroom_pyramid_all2_test = pyramid_all2;
% livingroom_pyramid_all3_test = pyramid_all3;

load('test_pyramids/store_testPyramid.mat');
store_pyramid_all_test = pyramid_all;
% store_pyramid_all2_test = pyramid_all2;
% store_pyramid_all3_test = pyramid_all3;

